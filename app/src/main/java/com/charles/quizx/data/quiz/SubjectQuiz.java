package com.charles.quizx.data.quiz;

import com.charles.quizx.data.question.MultiChoiceQuestion;
import java.util.ArrayList;
import java.util.List;

/**
 * Allows objects to be constructed which store a list of {@link MultiChoiceQuestion}s to make up a quiz
 */
public class SubjectQuiz
{

    private final List<MultiChoiceQuestion> questions;

    /**
     * Constructs a SubjectQuiz object with no questions
     */
    public SubjectQuiz()
    {
        questions = new ArrayList<>();
    }

    /**
     * @return The list of {@link MultiChoiceQuestion}s in the quiz
     */
    public List<MultiChoiceQuestion> getQuestions()
    {
        return questions;
    }

    /**
     * Adds a {@link MultiChoiceQuestion} to the list of questions
     * @param question The {@link MultiChoiceQuestion} to be added to the list of questions
     */
    public void addQuestion(MultiChoiceQuestion question)
    {
        questions.add(question);
    }

}
