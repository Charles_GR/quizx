package com.charles.quizx.data.scores;

import com.charles.quizx.data.score.Score;

/**
 * Defines a contract for objects which store a collection of {@link Score}s
 * @param <AddScoreParamType> Parameter type of the addScore method
 */
public interface Scores<AddScoreParamType>
{

    /**
     * Adds a {@link Score} to the collection of {@link Score}s
     * @param param Relates to the {@link Score} which is to be added
     * @return Whether it is true or false that a new {@link Score} was added
     */
    Boolean addScore(AddScoreParamType param);

    /**
     * Clears all the {@link Score}s stored in this Scores
     */
    void clearAllScores();

}
