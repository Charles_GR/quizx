package com.charles.quizx.data.quiz;

import java.util.HashMap;
import java.util.TreeSet;

/**
 * Allows objects to be constructed which store a mapping of subject names to {@link SubjectQuiz}zes
 */
public class SubjectQuizzes
{

    private final HashMap<String, SubjectQuiz> subjects;

    /**
     * Constructs a SubjectQuizzes object with no mappings
     */
    public SubjectQuizzes()
    {
        subjects = new HashMap<>();
    }

    /**
     * Sorts the subject names alphabetically then returns them
     * @return The subject names stored in this object sorted alphabetically
     */
    public TreeSet<String> getSubjectNames()
    {
        return new TreeSet<>(subjects.keySet());
    }

    /**
     * Gets the {@link SubjectQuiz} object for the given subject
     * @param subject - The subject used as a key to get the {@link SubjectQuiz} object
     * @return The {@link SubjectQuiz} object for the given subject
     */
    public SubjectQuiz getSubjectQuiz(String subject)
    {
        return subjects.get(subject);
    }

    /**
     * Adds a mapping of subject name and {@link SubjectQuiz} object to the collection
     * @param subject The subject of the questions
     * @param questions The {@link SubjectQuiz} object which contains the questions
     */
    public void addSubjectQuiz(String subject, SubjectQuiz questions)
    {
        subjects.put(subject, questions);
    }

}
