package com.charles.quizx.data.timer;

import android.os.SystemClock;

/**
 * Allows objects to be constructed which time how long a quiz takes to complete
 */
public class QuizTimer
{

    private QuizTimer pauseTimer;
    private boolean started;
    private boolean paused;
    private boolean finished;
    private float startTime;
    private float finishTime;
    private float pausedTime;

    /**
     * Constructs a new QuizTimer to time how long a quiz takes to complete
     */
    public QuizTimer()
    {
        pauseTimer = null;
        started = false;
        paused = false;
        finished = false;
        startTime = 0;
        finishTime = 0;
        pausedTime = 0;
    }

    /**
     * Starts the QuizTimer by recording the start time
     */
    public void start()
    {
        if(!started)
        {
            started = true;
            startTime = SystemClock.elapsedRealtime() / 1000f;
        }
    }

    /**
     * Stops the QuizTimer by recording the finish time
     */
    public void stop()
    {
        if(started && !paused)
        {
            finished = true;
            finishTime = SystemClock.elapsedRealtime() / 1000f;
        }
    }

    /**
     * Pauses the QuizTimer by starting a new pause timer
     */
    public void pause()
    {
        if(started && !paused)
        {
            paused = true;
            pauseTimer = new QuizTimer();
            pauseTimer.start();
        }
    }

    /**
     * Resumes the QuizTimer by stopping the pause timer and adding its running time to the paused time
     */
    public void resume()
    {
        if(started && paused)
        {
            paused = false;
            pauseTimer.stop();
            pausedTime += pauseTimer.calcRunningTime();
        }
    }

    /**
     * Calculates the running time of this QuizTimer by subtracting the paused time from the elapsed time
     * @return The running time in seconds
     */
    public float calcRunningTime()
    {
        if(!started || paused || !finished)
        {
            throw new IllegalStateException();
        }
        return finishTime - startTime - pausedTime;
    }

}