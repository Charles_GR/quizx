package com.charles.quizx.data.scores;

import com.charles.quizx.data.score.QuizScore;
import java.util.*;

/**
 * Allows objects to be constructed which keep track of the best {@link QuizScore} for each subject
 */
public class QuizScores implements Scores<QuizScore>
{

    private final TreeMap<String, QuizScore> quizScores;

    /**
     * Constructs an empty {@link QuizScores} object
     */
    public QuizScores()
    {
        quizScores = new TreeMap<>();
    }

    /**
     * @return The iterator of the {@link QuizScore}s stored in the {@link TreeMap}
     */
    public Iterator<QuizScore> getIterator()
    {
        return quizScores.values().iterator();
    }

    /**
     * Adds the given QuizScore to the collection of {@link QuizScore}s if it is the best of its subject
     * @param quizScore The {@link QuizScore} to be added to the collection of {@link QuizScore}s
     * @return Whether it is true or false that the {@link QuizScore} was added
     */
    @Override
    public Boolean addScore(QuizScore quizScore)
    {
        if(!quizScores.containsKey(quizScore.getSubject()) || quizScore.compareTo(quizScores.get(quizScore.getSubject())) < 0)
        {
            quizScores.put(quizScore.getSubject(), quizScore);
            return true;
        }
        return false;
    }

    /**
     * Clears the mapping of subjects to {@link QuizScore}s
     */
    @Override
    public void clearAllScores()
    {
        quizScores.clear();
    }

}