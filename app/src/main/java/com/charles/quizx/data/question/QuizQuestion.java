package com.charles.quizx.data.question;

/**
 * Allows objects to be constructed which represent quiz questions
 * @param <AnswerType> The type used for answers to the question
 */
public abstract class QuizQuestion<AnswerType>
{

    protected String question;
    protected AnswerType correctAnswer;

    /**
     * Constructs an empty QuizQuestion
     */
    protected QuizQuestion()
    {

    }

    /**
     * Constructs a QuizQuestion with the given question
     * @param question The question which is to be asked
     */
    protected QuizQuestion(String question)
    {
        this.question = question;
    }

    /**
     * @return The question which is to be asked
     */
    public String getQuestion()
    {
        return question;
    }

    /**
     * Checks if a given answer to the question is correct
     * @param answer The answer to check for correctness
     * @return Whether it is true or false that the answer given is correct
     */
    public boolean isCorrect(AnswerType answer)
    {
        return answer.equals(correctAnswer);
    }

}
