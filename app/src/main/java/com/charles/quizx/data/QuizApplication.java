package com.charles.quizx.data;

import android.app.Application;
import com.charles.quizx.R;
import com.charles.quizx.data.quiz.SubjectQuizzes;
import com.charles.quizx.data.scores.*;
import com.charles.quizx.data.timer.QuizTimer;
import com.google.gson.Gson;

/**
 * Allows the storage and access of data common to the entire application
 */
public class QuizApplication extends Application
{

    private static QuizApplication instance;

    private SubjectQuizzes subjectQuizzes;
    private SubjectScores subjectScores;
    private QuizScores quizScores;
    private QuizTimer quizTimer;
    private int questionsRequested;

    /**
     * Gets the singleton instance of QuizApplication
     * @return - The singleton instance of QuizApplication
     */
    public static QuizApplication getInstance()
    {
        return instance;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        instance = this;
        subjectQuizzes = new SubjectQuizzes();
        subjectScores = new SubjectScores();
        quizScores = new QuizScores();
        quizTimer = new QuizTimer();
        questionsRequested = 0;
    }

    /**
     * @return The {@link SubjectQuizzes} stored in the application
     */
    public SubjectQuizzes getSubjectQuizzes()
    {
        return subjectQuizzes;
    }

    /**
     * @return The {@link SubjectScores} stored in the application
     */
    public SubjectScores getSubjectScores()
    {
        return subjectScores;
    }

    /**
     * @return The {@link QuizScores} stored in the application
     */
    public QuizScores getQuizScores()
    {
        return quizScores;
    }

    /**
     * @return The {@link QuizTimer} stored in the application
     */
    public QuizTimer getQuizTimer()
    {
        return quizTimer;
    }

    /**
     * @return The number of questions requested by the user
     */
    public int getQuestionsRequested()
    {
        return questionsRequested;
    }

    /**
     * Sets the number of questions requested by the user
     * @param questionsRequested The number of questions requested
     */
    public void setQuestionsRequested(int questionsRequested)
    {
        if(questionsRequested >= getResources().getInteger(R.integer.min_number_of_questions) &&
           questionsRequested <= getResources().getInteger(R.integer.max_number_of_questions))
        {
            this.questionsRequested = questionsRequested;
        }
    }

    /**
     * Resets the {@link QuizTimer} by creating a new one
     */
    public void resetQuizTimer()
    {
        quizTimer = new QuizTimer();
    }

    /**
     * Loads the {@link Scores} objects from {@link String}s of JSON
     * @param subjectScoresJson The {@link String} of JSON representing the {@link SubjectScores} object
     * @param quizScoresJson The {@link String} of JSON representing the {@link QuizScores} object
     */
    public void loadQuizScores(String subjectScoresJson, String quizScoresJson)
    {
        subjectScores = (subjectScoresJson.length() == 0) ? new SubjectScores() : new Gson().fromJson(subjectScoresJson, SubjectScores.class);
        quizScores = (quizScoresJson.length() == 0) ? new QuizScores() : new Gson().fromJson(quizScoresJson, QuizScores.class);
    }

}