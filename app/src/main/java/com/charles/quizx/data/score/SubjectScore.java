package com.charles.quizx.data.score;

/**
 * Allows objects to be constructed which keep track of quiz performance for a single subject
 */
public class SubjectScore extends Score
{

    /**
     * Constructs a SubjectScore for the specified subject
     *
     * @param subject The subject that this SubjectScore is for
     */
    public SubjectScore(String subject)
    {
        super(subject);
    }

    /**
     * Updates the SubjectScore when a correct answer is given
     */
    public void addCorrectAnswer()
    {
        questionsAnswered++;
        questionsCorrect++;
    }

    /**
     * Updates the SubjectScore when an incorrect answer is given
     */
    public void addIncorrectAnswer()
    {
        questionsAnswered++;
    }

    /**
     * Converts this SubjectScore into a {@link String} containing the subject and the scores
     *
     * @return A {@link String} representation of this SubjectScore
     */
    @Override
    public String toString()
    {
        return subject + " (" + questionsCorrect + " / " + questionsAnswered +
                (questionsAnswered > 0 ? " = " + getFormattedPercentageCorrect() : "") + ")";
    }

    @Override
    public int compareTo(Score other)
    {
        SubjectScore otherScore = (SubjectScore) other;
        int compare = Float.compare(calcPercentageCorrect(), otherScore.calcPercentageCorrect());
        compare = (compare == 0 && questionsCorrect == 0) ? other.questionsAnswered - questionsAnswered : compare;
        compare = (compare == 0) ? questionsAnswered - other.questionsAnswered : compare;
        return compare;
    }

    @Override
    public boolean equals(Object other)
    {
        if(other instanceof SubjectScore)
        {
            SubjectScore score = (SubjectScore)other;
            return questionsAnswered == score.questionsAnswered && questionsCorrect == score.questionsCorrect;
        }
        return false;
    }

}