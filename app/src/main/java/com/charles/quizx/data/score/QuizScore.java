package com.charles.quizx.data.score;

/**
 * Allows objects to be constructed which keep track of quiz performance for a single quiz
 */
public class QuizScore extends Score
{

    private float timeTaken;

    /**
     * Constructs a QuizScore for the specified subject
     * @param subject The subject that this QuizScore is for
     * @param questionsAnswered The number of questions answered
     * @param questionsCorrect The number of questions correct
     * @param timeTaken The time in seconds taken to complete the quiz
     */
    public QuizScore(String subject, int questionsAnswered, int questionsCorrect, float timeTaken)
    {
        super(subject, questionsAnswered, questionsCorrect);
        this.timeTaken = timeTaken;
    }

    /**
     * @return The time taken rounded to one decimal place
     */
    private float getRoundedTimeTaken()
    {
        return Math.round(100f * timeTaken) / 100f;
    }

    @Override
    public String toString()
    {
        return subject + ": "+ questionsCorrect + " / " + questionsAnswered + " = " + getFormattedPercentageCorrect() +
                " in " + getRoundedTimeTaken() + " secs";
    }

    @Override
    public int compareTo(Score other)
    {
        QuizScore otherScore = (QuizScore) other;
        int compare = Float.compare(otherScore.calcPercentageCorrect(), calcPercentageCorrect());
        compare = (compare == 0 && questionsCorrect == 0) ? questionsAnswered - otherScore.questionsAnswered : compare;
        compare = (compare == 0) ? Float.compare(otherScore.questionsAnswered / otherScore.timeTaken, questionsAnswered / timeTaken) : compare;
        compare = (compare == 0) ? otherScore.questionsAnswered - questionsAnswered : compare;
        return compare;
    }

    @Override
    public boolean equals(Object other)
    {
        if(other instanceof QuizScore)
        {
            QuizScore score = (QuizScore)other;
            return questionsAnswered == score.questionsAnswered && questionsCorrect == score.questionsCorrect &&
                    timeTaken == score.timeTaken;
        }
        return false;
    }

}
