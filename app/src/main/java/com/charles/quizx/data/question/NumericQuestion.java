package com.charles.quizx.data.question;

/**
 * Allows objects to be constructed which represent numeric {@link QuizQuestion}s
 */
public class NumericQuestion extends QuizQuestion<Integer>
{

    private enum Operation
    {
        ADDITION, SUBTRACTION, MULTIPLICATION, DIVISION, SQUARE_ROOT
    }

    private static final Operation[] operations = Operation.values();

    /**
     * Constructs a numeric question with a random operation and random operands
     */
    public NumericQuestion()
    {
        Operation operation = operations[randomInRange(0, 4)];
        int operandA = randomInRange(1, 20);
        int operandB = randomInRange(1, 20);
        switch(operation)
        {
            case ADDITION:
                question = operandA + " + " + operandB + " = ?";
                correctAnswer = operandA + operandB;
                break;
            case SUBTRACTION:
                question = operandA + " x " + operandB + " = ?";
                correctAnswer = operandA * operandB;
                break;
            case MULTIPLICATION:
                question = operandA + " - " + operandB + " = ?";
                correctAnswer = operandA - operandB;
                break;
            case DIVISION:
                correctAnswer = randomInRange(1, 10);
                operandA =  correctAnswer * operandB;
                question = operandA + " / " + operandB + " = ?";
                break;
            case SQUARE_ROOT:
                correctAnswer = randomInRange(1, 10);
                operandA = correctAnswer * correctAnswer;
                question = "√" + operandA + " = ?";
                break;
        }
    }

    /**
     * Generates a random number within the given range
     * @param min The minimum allowed value of the random number
     * @param max The maximum allowed value of the random number
     * @return The generated random number
     */
    private int randomInRange(int min, int max)
    {
        return min + (int)(Math.random() * (max - min + 1));
    }

}

