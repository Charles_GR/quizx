package com.charles.quizx.data.question;

import java.util.*;

/**
 * Allows objects to be constructed which represent multiple choice {@link QuizQuestion}s
 */
public class MultiChoiceQuestion extends QuizQuestion<String>
{

	private final List<String> answers;

    /**
     * Constructs a MultiChoiceQuestion with the given question and no answers
     */
	public MultiChoiceQuestion(String question)
	{
        super(question);
		answers = new ArrayList<>();
	}

    /**
     * @return The different choices of answer available with the question
     */
	public List<String> getAnswers()
	{
		return answers;
	}

    /**
     * Adds an answer to the choices of answers available with the question
     * @param answer The answer to add to choice of answers
     * @param correct Whether it is true or false that this answer is correct
     */
	public void addAnswer(String answer, boolean correct)
	{
        answers.add(answer);
		correctAnswer = correct ? answer : correctAnswer;
	}

    /**
     * Jumbles the answer choices so that they appear in a random order
     */
	public void jumbleAnswers()
	{
		Collections.shuffle(answers);
	}

}