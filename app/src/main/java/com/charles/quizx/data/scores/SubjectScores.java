package com.charles.quizx.data.scores;

import com.charles.quizx.data.score.SubjectScore;
import java.util.*;

/**
 * Allows objects to be constructed which keep track of quiz performance, both overall and for each subject
 */
public class SubjectScores implements Scores<String>
{

    private SubjectScore overallScore;
    private final HashMap<String, SubjectScore> subjectScores;

    /**
     * Constructs a SubjectScores with the default subject of Maths included
     */
    public SubjectScores()
    {
        overallScore = new SubjectScore("Overall");
        subjectScores = new HashMap<>();
        subjectScores.put("Maths", new SubjectScore("Maths"));
    }

    /**
     * Gets the {@link SubjectScore} for the given subject name
     * @param subject The subject name to get the {@link SubjectScore} for
     * @return The {@link SubjectScore} for the given subject name
     */
    public SubjectScore getSubjectScore(String subject)
    {
        return subjectScores.get(subject);
    }

    /**
     * @return The overallScore {@link SubjectScore} which contains statitics for all subjectScores
     */
    public SubjectScore getOverallScore()
    {
        return overallScore;
    }

    /**
     * Gets the {@link SubjectScore}s which are statistically best
     * @return A list of the best {@link SubjectScore}s
     */
    public List<SubjectScore> getBestSubjectScores()
    {
        return getExtremeSubjectScores(true);
    }

    /**
     * Gets the {@link SubjectScore}s which are statistically worst
     * @return A list of the worst {@link SubjectScore}s
     */
    public List<SubjectScore> getWorstSubjectScores()
    {
        return getExtremeSubjectScores(false);
    }

    /**
     * Adds a {@link SubjectScore} for the given subject name if it does already have one
     * @param subject The subject name to map a {@link SubjectScore} to
     * @return Whether it is true or false that a new the {@link SubjectScore} was added
     */
    @Override
    public Boolean addScore(String subject)
    {
        if(!subjectScores.containsKey(subject))
        {
            subjectScores.put(subject, new SubjectScore(subject));
            return true;
        }
        return false;
    }

    /**
     * Updates the subjectScores when a correct answer is given for the given subject
     * @param subject The subject for which a correct answer was given
     */
    public void addCorrectAnswer(String subject)
    {
        overallScore.addCorrectAnswer();
        subjectScores.get(subject).addCorrectAnswer();
    }

    /**
     * Updates the subjectScores when an incorrect answer is given for the given subject
     * @param subject The subject for which an incorrect answer was given
     */
    public void addIncorrectAnswer(String subject)
    {
        overallScore.addIncorrectAnswer();
        subjectScores.get(subject).addIncorrectAnswer();
    }

    @Override
    public void clearAllScores()
    {
        overallScore = new SubjectScore("Overall");
        for(String subject : subjectScores.keySet())
        {
            subjectScores.put(subject, new SubjectScore(subject));
        }
    }

    /**
     * Converts a list of {@link SubjectScore}s at the extremes of statistical performance into a
     * {@link String} representation
     * @param extremeSubjectScores The extreme {@link SubjectScore}s
     * @return The String representing the extreme {@link SubjectScore}s
     */
    public static String extremeSubjectScoresToString(List<SubjectScore> extremeSubjectScores)
    {
        String text = "";
        Iterator<SubjectScore> iterator = extremeSubjectScores.iterator();
        while(iterator.hasNext())
        {
            text += iterator.next().getSubject();
            if(iterator.hasNext())
            {
                text += ", ";
            }
        }
        text += " (" + extremeSubjectScores.get(0).getQuestionsCorrect() + (" / ") +
                       extremeSubjectScores.get(0).getQuestionsAnswered() + " = " +
                       extremeSubjectScores.get(0).getFormattedPercentageCorrect() + ")";
        return text;
    }

    /**
     * Gets the {@link SubjectScore}s which have at least one question answered
     * @return A mapping of {@link SubjectScore}s with at least one question answered
     */
    private List<SubjectScore> getNonEmptySubjectScores()
    {
        List<SubjectScore> nonEmptySubjectScores = new ArrayList<>(subjectScores.values());
        Iterator<SubjectScore> iterator = nonEmptySubjectScores.iterator();
        while(iterator.hasNext())
        {
            if(iterator.next().getQuestionsAnswered() == 0)
            {
                iterator.remove();
            }
        }
        return nonEmptySubjectScores;
    }

    /**
     * Gets the {@link SubjectScore}s which are at the extremes of statistical performance
     * @param best True to return the subjectScores which are statistically best or false to return
     *             the subjectScores which are statistically worst
     * @return A list of the best or worst subject subjectScores depending on what is requested
     */
    private List<SubjectScore> getExtremeSubjectScores(boolean best)
    {
        List<SubjectScore> extremeSubjectScores = getNonEmptySubjectScores();
        Collections.sort(extremeSubjectScores, best ? Collections.reverseOrder() : null);
        Iterator<SubjectScore> iterator = extremeSubjectScores.iterator();
        while(iterator.hasNext())
        {
            if(!extremeSubjectScores.get(0).equals(iterator.next()))
            {
                iterator.remove();
            }
        }
        return extremeSubjectScores;
    }

}