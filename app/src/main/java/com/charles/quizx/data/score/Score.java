package com.charles.quizx.data.score;

import java.util.Locale;

/**
 * Allows objects to be constructed which keep track of quiz performance
 */
public abstract class Score implements Comparable<Score>
{

    protected final String subject;
    protected int questionsAnswered;
    protected int questionsCorrect;

    /**
     * Constructs a Score for the specified subject
     * @param subject The subject that this Score is for
     */
    protected Score(String subject)
    {
        this.subject = subject;
        questionsAnswered = 0;
        questionsCorrect = 0;
    }

    /**
     * Constructs a Score for the specified subject
     * @param subject The subject that this Score is for
     * @param questionsAnswered The number of questions answered
     * @param questionsCorrect The number of questions correct
     */
    protected Score(String subject, int questionsAnswered, int questionsCorrect)
    {
        this.subject = subject;
        this.questionsAnswered = questionsAnswered;
        this.questionsCorrect = questionsCorrect;
    }

    /**
     * @return The subject that this Score is for
     */
    public String getSubject()
    {
        return subject;
    }

    /**
     * @return The number of questions answered
     */
    public int getQuestionsAnswered()
    {
        return questionsAnswered;
    }

    /**
     * @return The number of questions correct
     */
    public int getQuestionsCorrect()
    {
        return questionsCorrect;
    }

    /**
     * Calculates the percentage of questions correct out of questions answered
     * @return The calculated percentage correct
     */
    protected float calcPercentageCorrect()
    {
        return questionsAnswered == 0 ? 0 : 100f * questionsCorrect / questionsAnswered;
    }

    /**
     * Calculates the percentage correct then formats it to one decimal place
     * @return The formatted percentage correct
     */
    public String getFormattedPercentageCorrect()
    {
        float percentage = Math.round(10f * calcPercentageCorrect()) / 10f;
        return (percentage == (long)percentage) ? String.format(Locale.UK, "%d", (long)percentage) + "%"
                                                : String.format("%s", percentage) + "%";
    }

    @Override
    public abstract String toString();

    /**
     * Compares this Score to another Score to determine sorting order.
     * @param other The other Score to compare this Score with
     * @return The result of the comparison which is negative if this Score should come first,
     * positive if the other Score should come first and zero if it does not matter.
     */
    @Override
    public abstract int compareTo(Score other);

    /**
     * Compares this Score to another object to test for equality.
     * @param other The other object to compare this Score with
     * @return Whether it is true or false that this Score is equal to the other object
     */
    @Override
    public abstract boolean equals(Object other);

}
