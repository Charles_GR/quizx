package com.charles.quizx.view.question;

import android.app.Activity;
import android.os.Bundle;
import android.view.View.OnClickListener;
import android.widget.*;
import com.charles.quizx.R;
import com.charles.quizx.data.QuizApplication;
import com.charles.quizx.data.question.QuizQuestion;
import com.charles.quizx.data.score.QuizScore;

/**
 * Displays a {@link QuizQuestion} on the screen and allows the user to answer it
 * @param <QuestionType> The implementing type of the {@link QuizQuestion}
 * @param <AnswerType> The type of the answer to the {@link QuizQuestion}
 */
public abstract class QuizQuestionActivity<QuestionType extends QuizQuestion<AnswerType>, AnswerType> extends Activity implements OnClickListener
{

    protected String subject;
    protected QuestionType currentQuestion;
    protected int questionsAnswered;
    protected int questionsCorrect;
    protected Button btnSubmit;
    protected TextView tvQuestion;
    protected TextView tvSummary;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        btnSubmit = (Button)findViewById(savedInstanceState.getInt("submit"));
        tvQuestion = (TextView)findViewById(savedInstanceState.getInt("question"));
        tvSummary = (TextView)findViewById(savedInstanceState.getInt("summary"));
        btnSubmit.setOnClickListener(this);
        generateQuestion();
        generateSummary();
        QuizApplication.getInstance().getQuizTimer().start();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        QuizApplication.getInstance().getQuizTimer().pause();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        QuizApplication.getInstance().getQuizTimer().resume();
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        QuizApplication.getInstance().resetQuizTimer();
    }

    /**
     * Generates a {@link QuizQuestion} and displays it to the user
     */
    protected abstract void generateQuestion();

    /**
     * Generates a summary of quiz progress and displays it to the user
     */
    protected void generateSummary()
    {
        String summary = getResources().getString(R.string.quiz_summary, questionsCorrect, questionsAnswered, QuizApplication.getInstance().getQuestionsRequested());
        tvSummary.setText(summary);
    }

    /**
     * Checks if the given answer is correct and responds accordingly
     * @param answer The answer to check for correctness
     */
    protected void checkAnswer(AnswerType answer)
    {
        if(currentQuestion.isCorrect(answer))
        {
            Toast.makeText(this, "Correct!", Toast.LENGTH_SHORT).show();
            QuizApplication.getInstance().getSubjectScores().addCorrectAnswer(subject);
            questionsCorrect++;
        }
        else
        {
            Toast.makeText(this, "Incorrect!", Toast.LENGTH_SHORT).show();
            QuizApplication.getInstance().getSubjectScores().addIncorrectAnswer(subject);
        }
    }

    /**
     * Advances the quiz by finishing it if all the questions have been answered or otherwise generating
     * a new question and a new quiz summary
     */
    protected void advanceQuiz()
    {
        if(++questionsAnswered == QuizApplication.getInstance().getQuestionsRequested())
        {
            finishQuiz();
        }
        else
        {
            generateQuestion();
            generateSummary();
        }
    }

    /**
     * Finishes the quiz by checking the quiz performance then finishing the activity
     */
    protected void finishQuiz()
    {
        QuizApplication.getInstance().getQuizTimer().stop();
        float time =  QuizApplication.getInstance().getQuizTimer().calcRunningTime();
        QuizScore quizScore = new QuizScore(subject, questionsAnswered, questionsCorrect, time);
        if(QuizApplication.getInstance().getQuizScores().addScore(quizScore))
        {
            Toast.makeText(this, "New best quiz score added!", Toast.LENGTH_SHORT).show();
        }
        QuizApplication.getInstance().resetQuizTimer();
        finish();
    }

}
