package com.charles.quizx.view.question;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.charles.quizx.R;
import com.charles.quizx.data.question.MultiChoiceQuestion;
import com.charles.quizx.data.question.NumericQuestion;

/**
 * Displays a {@link NumericQuestion} on the screen and allows the user to answer it
 */
public class NumericQuestionActivity extends QuizQuestionActivity<NumericQuestion, Integer>
{

    private EditText etAnswer;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        subject = "Maths";
        setContentView(R.layout.numeric_question_activity);
        etAnswer = (EditText)findViewById(R.id.etAnswer);
        savedInstanceState = new Bundle();
        savedInstanceState.putInt("question", R.id.tvQuestion);
        savedInstanceState.putInt("summary", R.id.tvSummary);
        savedInstanceState.putInt("submit", R.id.btnSubmit);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void generateQuestion()
    {
        currentQuestion = new NumericQuestion();
        tvQuestion.setText(currentQuestion.getQuestion());
        etAnswer.setText("");
    }

    @Override
    public void onClick(View v)
    {
        if(v.getId() ==  R.id.btnSubmit)
        {
            if(etAnswer.getText().toString().length() == 0)
            {
                Toast.makeText(this, "Enter an answer!", Toast.LENGTH_SHORT).show();
                return;
            }
            checkAnswer(Integer.valueOf(etAnswer.getText().toString()));
            advanceQuiz();
        }
    }

}