package com.charles.quizx.view.question;

import android.os.Bundle;
import android.view.View;
import android.widget.*;
import com.charles.quizx.R;
import com.charles.quizx.data.QuizApplication;
import com.charles.quizx.data.question.MultiChoiceQuestion;
import java.util.List;

/**
 * Displays a {@link MultiChoiceQuestion} on the screen and allows the user to answer it
 */
public class MultiChoiceQuestionActivity extends QuizQuestionActivity<MultiChoiceQuestion, String>
{

    private List<MultiChoiceQuestion> questions;
    private RadioGroup answerChoices;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        subject = getIntent().getStringExtra("subject");
        setContentView(R.layout.multi_choice_question_activity);
        questions = QuizApplication.getInstance().getSubjectQuizzes().getSubjectQuiz(subject).getQuestions();
        answerChoices = (RadioGroup)findViewById(R.id.answerChoices);
        savedInstanceState = new Bundle();
        savedInstanceState.putInt("question", R.id.tvQuestion);
        savedInstanceState.putInt("summary", R.id.tvSummary);
        savedInstanceState.putInt("submit", R.id.btnSubmit);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void generateQuestion()
    {
        answerChoices.removeAllViews();
        answerChoices.clearCheck();
        currentQuestion = questions.remove((int)(Math.random() * questions.size()));
        currentQuestion.jumbleAnswers();
        tvQuestion.setText(currentQuestion.getQuestion());
        for(int i = 0; i < currentQuestion.getAnswers().size(); i++)
        {
            RadioButton rbAnswer = new RadioButton(this);
            rbAnswer.setText(currentQuestion.getAnswers().get(i));
            rbAnswer.setId(i + 1);
            answerChoices.addView(rbAnswer);
        }
    }

    @Override
    public void onClick(View v)
    {
        if(v.getId() == R.id.btnSubmit)
        {
            int id = answerChoices.getCheckedRadioButtonId();
            if(id == -1)
            {
                Toast.makeText(this, "Select an answer!", Toast.LENGTH_SHORT).show();
                return;
            }
            RadioButton rbSelected = (RadioButton)findViewById(id);
            checkAnswer(rbSelected.getText().toString());
            advanceQuiz();
        }
    }

}