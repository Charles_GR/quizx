package com.charles.quizx.view.quiz;

import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.charles.quizx.R;
import com.charles.quizx.data.QuizApplication;
import com.charles.quizx.data.score.QuizScore;
import com.charles.quizx.data.scores.QuizScores;
import java.util.*;

/**
 * Displays the best {@link QuizScore} onscreen for each different subject
 */
public class QuizScoresActivity extends Activity implements OnClickListener
{

    private ListView lvBestQuizScores;
    private Button btnClearScores;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quiz_scores_activity);
        lvBestQuizScores = (ListView)findViewById(R.id.lvBestQuizScores);
        btnClearScores = (Button)findViewById(R.id.btnClearScores);
        btnClearScores.setOnClickListener(this);
        updateQuizScores();
    }

    @Override
    public void onClick(View v)
    {
        if(v.getId() == R.id.btnClearScores)
        {
            QuizApplication.getInstance().getQuizScores().clearAllScores();
            finish();
        }
    }

    /**
     * Updates the onscreen display of {@link QuizScores}
     */
    private void updateQuizScores()
    {
        List<String> quizScoreStrings = new ArrayList<>();
        Iterator<QuizScore> it = QuizApplication.getInstance().getQuizScores().getIterator();
        if(!it.hasNext())
        {
            Toast.makeText(this, "No quiz scores yet!", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        while(it.hasNext())
        {
            quizScoreStrings.add(it.next().toString());
        }
        lvBestQuizScores.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, quizScoreStrings));
    }

}
