package com.charles.quizx.view.quiz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.SeekBar.OnSeekBarChangeListener;
import com.charles.quizx.R;
import com.charles.quizx.data.QuizApplication;
import com.charles.quizx.data.quiz.SubjectQuizzes;
import com.charles.quizx.data.score.SubjectScore;
import com.charles.quizx.data.scores.SubjectScores;
import com.charles.quizx.processing.task.*;
import com.charles.quizx.view.question.MultiChoiceQuestionActivity;
import com.charles.quizx.view.question.NumericQuestionActivity;
import java.util.*;

/**
 * Displays quiz settings and {@link SubjectScores} onscreen and allows a quiz to be started
 */
public class QuizActivity extends Activity implements OnClickListener, OnSeekBarChangeListener, OnItemSelectedListener
{

    private static QuizActivity instance;

    private Button btnStartQuiz;
    private Button btnQuizScores;
    private Button btnClearScores;
    private Spinner spnSubject;
    private SeekBar sbNumberOfQuestions;
    private TextView tvNumberOfQuestions;
    private TextView tvOverallInfo;
    private TextView tvSelectedSubjectInfo;
    private TextView tvBestSubjectsInfo;
    private TextView tvWorstSubjectsInfo;

    /**
     * @return - The singleton instance of QuizActivity
     */
    public static QuizActivity getInstance()
    {
        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quiz_activity);
        instance = this;
        btnStartQuiz = (Button)findViewById(R.id.btnStartQuiz);
        btnQuizScores = (Button)findViewById(R.id.btnQuizScores);
        btnClearScores = (Button)findViewById(R.id.btnClearScores);
        spnSubject = (Spinner)findViewById(R.id.spnSubject);
        sbNumberOfQuestions = (SeekBar)findViewById(R.id.sbNumberOfQuestions);
        tvNumberOfQuestions = (TextView)findViewById(R.id.tvNumberOfQuestions);
        tvOverallInfo = (TextView)findViewById(R.id.tvOverallInfo);
        tvSelectedSubjectInfo = (TextView)findViewById(R.id.tvSelectedSubjectInfo);
        tvBestSubjectsInfo = (TextView)findViewById(R.id.tvBestSubjectsInfo);
        tvWorstSubjectsInfo = (TextView)findViewById(R.id.tvWorstSubjectsInfo);
        btnStartQuiz.setOnClickListener(this);
        btnQuizScores.setOnClickListener(this);
        btnClearScores.setOnClickListener(this);
        spnSubject.setOnItemSelectedListener(this);
        sbNumberOfQuestions.setOnSeekBarChangeListener(this);
        tvNumberOfQuestions.setText(String.valueOf(sbNumberOfQuestions.getProgress() + 1));
        QuizApplication.getInstance().setQuestionsRequested(sbNumberOfQuestions.getProgress() + 1);
        new LoadScoresTask().execute();
        new LoadSubjectQuizzesTask().execute();
        new LoadPreferencesTask().execute();
    }

    @Override
    public void onPause()
    {
        super.onPause();
        new SaveScoresTask().execute();
        new SavePreferencesTask().execute(spnSubject.getSelectedItem() == null ? "" : spnSubject.getSelectedItem().toString(),
                sbNumberOfQuestions.getProgress());
    }

    @Override
    public void onResume()
    {
        super.onResume();
        updateAllScores();
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnStartQuiz:
                if(spnSubject.getSelectedItem().toString().equals("Maths"))
                {
                    Intent intent = new Intent(this, NumericQuestionActivity.class);
                    intent.putExtra("subject", "Maths");
                    startActivity(intent);
                }
                else
                {
                    Intent intent = new Intent(this, MultiChoiceQuestionActivity.class);
                    intent.putExtra("subject", spnSubject.getSelectedItem().toString());
                    startActivity(intent);
                }
                break;
            case R.id.btnQuizScores:
                startActivity(new Intent(this, QuizScoresActivity.class));
                break;
            case R.id.btnClearScores:
                QuizApplication.getInstance().getSubjectScores().clearAllScores();
                updateAllScores();
                break;
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar)
    {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar)
    {

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
    {
        if(seekBar == sbNumberOfQuestions)
        {
            tvNumberOfQuestions.setText(String.valueOf(progress + getResources().getInteger(R.integer.min_number_of_questions)));
            QuizApplication.getInstance().setQuestionsRequested(progress + getResources().getInteger(R.integer.min_number_of_questions));
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        if(parent == spnSubject)
        {
            updateSelectedSubject();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {

    }

    /**
     * Updates the user interface components according to the preferences
     * @param selectedSubject Which subject of the spinner should be selected
     * @param seekBarProgress The progress that the seekBar should display
     */
    @SuppressWarnings("unchecked")
    public void updatePreferences(String selectedSubject, int seekBarProgress)
    {
        spnSubject.setSelection(((ArrayAdapter<String>)spnSubject.getAdapter()).getPosition(selectedSubject));
        sbNumberOfQuestions.setProgress(seekBarProgress);
    }

    /**
     * Fills the spinner of subjects so that subjects in {@link SubjectQuizzes} can be selected
     */
    public void updateQuizSubjects()
    {
        TreeSet<String> subjectNames = QuizApplication.getInstance().getSubjectQuizzes().getSubjectNames();
        subjectNames.add("Maths");
        spnSubject.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, new ArrayList<>(subjectNames)));
    }

    /**
     * Updates the onscreen display of {@link SubjectScores}
     */
    public void updateAllScores()
    {
        updateOverall();
        updateSelectedSubject();
        updateBestSubjects();
        updateWorstSubjects();
    }

    /**
     * Updates the onscreen display of the {@link SubjectScore} for overall performance
     */
    private void updateOverall()
    {
        SubjectScore overall = QuizApplication.getInstance().getSubjectScores().getOverallScore();
        String text = overall.getQuestionsCorrect() + " / " + overall.getQuestionsAnswered();
        if(QuizApplication.getInstance().getSubjectScores().getOverallScore().getQuestionsAnswered() > 0)
        {
            text += " = " + overall.getFormattedPercentageCorrect();
        }
        tvOverallInfo.setText(text);
    }

    /**
     * Updates the onscreen display of the selected subject and its {@link SubjectScore}
     */
    private void updateSelectedSubject()
    {
        if(spnSubject.getSelectedItem() == null)
        {
            tvSelectedSubjectInfo.setText(R.string.subject_scores_none);
        }
        else
        {
            String subject = spnSubject.getSelectedItem().toString();
            SubjectScore score = QuizApplication.getInstance().getSubjectScores().getSubjectScore(subject);
            tvSelectedSubjectInfo.setText(score == null ? subject + " (0 / 0)" : score.toString());
        }
    }

    /**
     * Updates the onscreen display of the best subjects and their {@link SubjectScore}s
     */
    private void updateBestSubjects()
    {
        updateExtremeSubjects(QuizApplication.getInstance().getSubjectScores().getBestSubjectScores(), tvBestSubjectsInfo);
    }

    /**
     * Updates the onscreen display of the worst subjects and their {@link SubjectScore}s
     */
    private void updateWorstSubjects()
    {
        updateExtremeSubjects(QuizApplication.getInstance().getSubjectScores().getWorstSubjectScores(), tvWorstSubjectsInfo);
    }

    /**
     * Updates the onscreen display of subjects at the extremes of performance and their {@link SubjectScore}s
     * @param extremeSubjects The list of extreme subjects to display
     * @param textView The textView used to display the extreme subjects
     */
    private void updateExtremeSubjects(List<SubjectScore> extremeSubjects, TextView textView)
    {
        switch(extremeSubjects.size())
        {
            case 0:
                textView.setText(R.string.subject_scores_none);
                break;
            case 1:
                textView.setText(extremeSubjects.get(0).toString());
                break;
            default:
                textView.setText(SubjectScores.extremeSubjectScoresToString(extremeSubjects));
                break;
        }
    }

}