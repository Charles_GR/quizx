package com.charles.quizx.processing.util;

import com.charles.quizx.data.QuizApplication;
import java.io.*;

/**
 * Allows objects to be constructed which read the contents of a resource file
 */
public class ResourceFileReader
{

    private final int resourceFileID;

    /**
     * Constructs a ResourceFileReader to read from the given resource file
     * @param resourceFileID The ID of the resource file to be read
     */
    public ResourceFileReader(int resourceFileID)
    {
        this.resourceFileID = resourceFileID;
    }

    /**
     * Reads the raw resource file from the stored resource file ID and returns the contents
     * @return The contents of the file that has been read
     * @throws IOException
     */
    public String readRawResourceFile() throws IOException
    {
        BufferedReader bufferedReader = null;
        try
        {
            InputStream inputStream = QuizApplication.getInstance().getResources().openRawResource(resourceFileID);
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder fileContents = new StringBuilder();
            String line = bufferedReader.readLine();
            while(line != null)
            {
                fileContents.append(line);
                line = bufferedReader.readLine();
            }
            return fileContents.toString();
        }
        finally
        {
            try
            {
                if(bufferedReader != null)
                {
                    bufferedReader.close();
                }
            }
            catch(IOException ex)
            {

            }
        }
    }

}