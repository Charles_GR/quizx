package com.charles.quizx.processing.task;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;
import com.charles.quizx.data.QuizApplication;
import com.charles.quizx.data.scores.Scores;
import com.charles.quizx.view.quiz.QuizActivity;
import com.google.gson.Gson;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Allows objects to be constructed which save {@link Scores} objects to file on a background thread
 */
public class SaveScoresTask extends AsyncTask<Void, String, Void>
{

    @Override
    protected Void doInBackground(Void... params)
    {
        writeScores(QuizApplication.getInstance().getSubjectScores(), "SubjectScores.log");
        writeScores(QuizApplication.getInstance().getQuizScores(), "QuizScores.log");
        return null;
    }

    @Override
    protected void onProgressUpdate(String... values)
    {
        super.onProgressUpdate(values);
        Toast.makeText(QuizActivity.getInstance(), values[0], Toast.LENGTH_SHORT).show();
    }

    private void writeScores(Scores scores, String fileName)
    {
        FileOutputStream fileOutputStream = null;
        try
        {
            fileOutputStream = QuizApplication.getInstance().openFileOutput(fileName, Context.MODE_PRIVATE);
            fileOutputStream.write(new Gson().toJson(scores).getBytes());
        }
        catch(IOException ex)
        {
            publishProgress("Error saving scores!");
        }
        finally
        {
            try
            {
                if(fileOutputStream != null)
                {
                    fileOutputStream.close();
                }
            }
            catch(Exception e)
            {

            }
        }
    }

}
