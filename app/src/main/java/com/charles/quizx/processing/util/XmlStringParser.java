package com.charles.quizx.processing.util;

import com.charles.quizx.data.QuizApplication;
import com.charles.quizx.data.question.MultiChoiceQuestion;
import com.charles.quizx.data.quiz.SubjectQuiz;
import com.charles.quizx.data.quiz.SubjectQuizzes;
import com.charles.quizx.data.score.SubjectScore;
import com.charles.quizx.data.scores.SubjectScores;
import java.io.IOException;
import java.io.StringReader;
import org.xmlpull.v1.*;

/**
 * Allows objects to be constructed which parse a {@link String} of XML into a {@link SubjectQuiz}
 */
public class XmlStringParser
{

    private static final String ANSWER_TAG_NAME = "answer";
    private static final String ASK_TAG_NAME = "ask";
    private static final String QUESTION_TAG_NAME = "question";
    private static final String QUESTIONS_TAG_NAME = "questions";

    private final XmlPullParser xmlPullParser;
    private String currentSubject;
    private SubjectQuiz currentSubjectQuiz;
    private MultiChoiceQuestion currentQuestion;

    /**
     * Constructs an XmlStringParser object for the given input String
     * @param input The {@link String} of XML to be parsed into a {@link SubjectQuiz}
     * @throws XmlPullParserException
     */
    public XmlStringParser(String input) throws XmlPullParserException
    {
        xmlPullParser = XmlPullParserFactory.newInstance().newPullParser();
        xmlPullParser.setInput(new StringReader(input));
    }

    /**
     * Parses the XML string stored in this XmlStringParser into a {@link SubjectQuiz} and stores it in the
     * {@link SubjectQuizzes}. Also adds a new {@link SubjectScore} to {@link SubjectScores} for the subject of the quiz
     * if one has not been created already.
     * @throws IOException
     * @throws XmlPullParserException
     */
    public void parseXmlString() throws IOException, XmlPullParserException
    {
        int eventType = xmlPullParser.getEventType();
        while(eventType != XmlPullParser.END_DOCUMENT)
        {
            try
            {
                switch(eventType)
                {
                    case XmlPullParser.START_TAG:
                        parseXmlStartTag();
                        break;
                    case XmlPullParser.END_TAG:
                        parseXmlEndTag();
                        break;
                }
            }
            catch(NullPointerException ex)
            {
                throw new XmlPullParserException("");
            }
            eventType = xmlPullParser.next();
        }
    }

    /**
     * Parses the XML start tag of an element
     * @throws IOException
     * @throws XmlPullParserException
     */
    private void parseXmlStartTag() throws IOException, XmlPullParserException
    {
        switch(xmlPullParser.getName())
        {
            case QUESTIONS_TAG_NAME:
                currentSubject = xmlPullParser.getAttributeValue(null, "subject");
                currentSubjectQuiz = new SubjectQuiz();
                break;
            case ASK_TAG_NAME:
                xmlPullParser.next();
                currentQuestion = new MultiChoiceQuestion(xmlPullParser.getText().trim());
                break;
            case ANSWER_TAG_NAME:
                boolean correct = Boolean.parseBoolean(xmlPullParser.getAttributeValue(null, "correct"));
                xmlPullParser.next();
                currentQuestion.addAnswer(xmlPullParser.getText().trim(), correct);
                break;
        }
    }

    /**
     * Parses the XML end tag of an element
     */
    private void parseXmlEndTag()
    {
        switch(xmlPullParser.getName())
        {
            case QUESTION_TAG_NAME:
                if(currentQuestion == null)
                {
                    throw new NullPointerException();
                }
                currentSubjectQuiz.addQuestion(currentQuestion);
                break;
            case QUESTIONS_TAG_NAME:
                if(currentSubject == null || currentSubjectQuiz == null)
                {
                    throw new NullPointerException();
                }
                QuizApplication.getInstance().getSubjectQuizzes().addSubjectQuiz(currentSubject, currentSubjectQuiz);
                QuizApplication.getInstance().getSubjectScores().addScore(currentSubject);
                break;
        }
    }

}