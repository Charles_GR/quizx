package com.charles.quizx.processing.task;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import com.charles.quizx.data.QuizApplication;
import com.charles.quizx.view.quiz.QuizActivity;

/**
 * Allows objects to be constructed which load quiz preferences from file on a background thread
 */
public class LoadPreferencesTask extends AsyncTask<Void, Void, Object[]>
{

    @Override
    protected Object[] doInBackground(Void... params)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(QuizApplication.getInstance());
        String spinnerSelectedItem = preferences.getString("SpinnerSelectedItem", "");
        int seekBarProgress = preferences.getInt("SeekBarProgress", 3);
        return new Object[]{spinnerSelectedItem, seekBarProgress};
    }

    @Override
    protected void onPostExecute(Object... params)
    {
        super.onPostExecute(params);
        QuizActivity.getInstance().updatePreferences((String)params[0], (int)params[1]);
    }

}
