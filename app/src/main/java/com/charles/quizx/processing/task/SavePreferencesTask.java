package com.charles.quizx.processing.task;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import com.charles.quizx.data.QuizApplication;

/**
 * Allows objects to be constructed which save quiz preferences to file on a background thread
 */
public class SavePreferencesTask extends AsyncTask<Object, Void, Void>
{

    @Override
    protected Void doInBackground(Object... params)
    {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(QuizApplication.getInstance()).edit();
        editor.putString("SpinnerSelectedItem", (String)params[0]);
        editor.putInt("SeekBarProgress", (int)params[1]);
        editor.apply();
        return null;
    }

}
