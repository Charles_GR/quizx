package com.charles.quizx.processing.task;

import android.os.AsyncTask;
import android.widget.Toast;
import com.charles.quizx.data.QuizApplication;
import com.charles.quizx.data.scores.Scores;
import com.charles.quizx.view.quiz.QuizActivity;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Allows objects to be constructed which load {@link Scores} objects from file on a background thread
 */
public class LoadScoresTask extends AsyncTask<Void, String, Void>
{

    @Override
    protected Void doInBackground(Void... params)
    {
        String subjectScoresJson = readScores("SubjectScores.log");
        String quizScoresJson = readScores("QuizScores.log");
        QuizApplication.getInstance().loadQuizScores(subjectScoresJson, quizScoresJson);
        return null;
    }

    @Override
    protected void onProgressUpdate(String... values)
    {
        super.onProgressUpdate(values);
        Toast.makeText(QuizActivity.getInstance(), values[0], Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPostExecute(Void aVoid)
    {
        QuizActivity.getInstance().updateAllScores();
    }

    private String readScores(String fileName)
    {
        FileInputStream fileInputStream = null;
        String fileContents = "";
        try
        {
            fileInputStream = QuizApplication.getInstance().openFileInput(fileName);
            int ch = fileInputStream.read();
            while(ch != -1)
            {
                fileContents += Character.toString((char)ch);
                ch = fileInputStream.read();
            }
        }
        catch(IOException e)
        {
            publishProgress("Error loading scores!");
        }
        finally
        {
            try
            {
                if(fileInputStream != null)
                {
                    fileInputStream.close();
                }
            }
            catch(Exception e)
            {

            }
        }
        return fileContents;
    }

}