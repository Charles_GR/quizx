package com.charles.quizx.processing.task;

import android.os.AsyncTask;
import android.widget.Toast;
import com.charles.quizx.R;
import com.charles.quizx.data.QuizApplication;
import com.charles.quizx.data.quiz.SubjectQuizzes;
import com.charles.quizx.processing.util.ResourceFileReader;
import com.charles.quizx.processing.util.XmlStringParser;
import com.charles.quizx.view.quiz.QuizActivity;
import org.xmlpull.v1.XmlPullParserException;
import java.io.IOException;
import java.util.*;

/**
 * Allows objects to be constructed which load a {@link SubjectQuizzes} from raw resource files on a background thread
 */
public class LoadSubjectQuizzesTask extends AsyncTask<Void, String, Void>
{

    private static final int[] resourceFiles = {R.raw.biology, R.raw.business, R.raw.chemistry,
            R.raw.food_and_drink, R.raw.geography, R.raw.history, R.raw.music, R.raw.physics};

    @Override
    protected Void doInBackground(Void... params)
    {
        for(int resourceFile : resourceFiles)
        {
            try
            {
                String fileContents = new ResourceFileReader(resourceFile).readRawResourceFile();
                new XmlStringParser(fileContents).parseXmlString();
            }
            catch(IOException | XmlPullParserException ex)
            {
                String fileName = QuizApplication.getInstance().getResources().getResourceName(resourceFile).split("/")[1] + ".xml";
                publishProgress("Error reading or parsing file: " + fileName + "!");
            }
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(String... values)
    {
        super.onProgressUpdate(values);
        Toast.makeText(QuizActivity.getInstance(), values[0], Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPostExecute(Void aVoid)
    {
        super.onPostExecute(aVoid);
        QuizActivity.getInstance().updateQuizSubjects();
    }

}